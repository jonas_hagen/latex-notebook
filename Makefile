# name of project
NAME = notebook

pdf: notes.tex
	pdflatex -interaction=nonstopmode $(NAME).tex
	evince $(NAME).pdf &

.PHONY: notes.tex
notes.tex:
	./get_notes

.PHONY: new
new:
	./new

.PHONY: clean
clean:
	rm $(NAME).pdf
	rm notes.tex
	rm *.aux
	rm $(NAME).log
	rm $(NAME).toc
	rm $(NAME).out

all: pdf
